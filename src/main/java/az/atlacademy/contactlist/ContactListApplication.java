package az.atlacademy.contactlist;

import az.atlacademy.contactlist.entity.manytomany.Product;
import az.atlacademy.contactlist.entity.manytomany.Store;
import az.atlacademy.contactlist.repository.ContactRepository;
import az.atlacademy.contactlist.repository.PersonRepository;
import az.atlacademy.contactlist.repository.ProductRepository;
import az.atlacademy.contactlist.repository.StoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;

@SpringBootApplication
@RequiredArgsConstructor
@Transactional
public class ContactListApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ContactListApplication.class, args);
    }

    private final ContactRepository contactRepository;
    private final PersonRepository personRepository;
    private final ProductRepository productRepository;
    private final StoreRepository storeRepository;


    @Override
    public void run(String... args) throws Exception {

       /* Person p = new Person();
        p.setName("Anar");
        p.setSurname("Xocayev");
        p = personRepository.save(p);

        Contact c1=new Contact();
        c1.setValue("0556122116");
        c1.setContactType(ContactType.PHONE);
        c1.setPerson(p);
        contactRepository.save(c1);

        Contact c2=new Contact();
        c2.setValue("0512297762");
        c2.setContactType(ContactType.PHONE);
        c2.setPerson(p);
        contactRepository.save(c2);

        Contact c3=new Contact();
        c3.setValue("anarxocayev@gmail.com");
        c3.setContactType(ContactType.EMAIL);
        c3.setPerson(p);
        contactRepository.save(c3);

        Contact c4=new Contact();
        c4.setValue("anar.xocayev");
        c4.setContactType(ContactType.SKYPE);
        c4.setPerson(p);
        contactRepository.save(c4);*/

    //    Person p=personRepository.findById(1L).get();
      //  System.out.println(p);
        Store s=storeRepository.findById(2L).get();
        System.out.println(s);

        Product p=productRepository.findById(22L).get();
        System.out.println(p);
        System.out.println(p.getStores());


    }
}
