package az.atlacademy.contactlist.controller;

import az.atlacademy.contactlist.entity.manytomanythirdtable.Student;
import az.atlacademy.contactlist.entity.manytomanythirdtable.dto.StudentDto;
import az.atlacademy.contactlist.entity.manytomanythirdtable.payload.StudentPayload;
import az.atlacademy.contactlist.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @GetMapping
    public ResponseEntity<List<StudentDto>> findall() {

        return ResponseEntity.status(HttpStatus.OK).body(studentService.findAll());
    }

    @PostMapping
    public ResponseEntity<StudentDto> create(@RequestBody StudentPayload studentPayload) {

        return ResponseEntity.status(HttpStatus.CREATED).body(studentService.create(studentPayload));
    }
    @DeleteMapping
    public ResponseEntity delete(@RequestBody StudentPayload studentPayload) {
        studentService.delete(studentPayload);
        return ResponseEntity.ok().build();
    }
}
