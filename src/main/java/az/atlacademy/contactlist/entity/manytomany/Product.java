package az.atlacademy.contactlist.entity.manytomany;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    @ManyToMany(mappedBy="products")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<Store> stores = new HashSet<Store>();
}


/*1:STORE->N:PRODUCT
N:STORE ->1:PRODUCT


M:N


stores products

  store_products
     id
     product_id
     store_id

 */


