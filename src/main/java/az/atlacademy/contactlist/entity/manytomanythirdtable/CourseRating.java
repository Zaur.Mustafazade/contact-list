package az.atlacademy.contactlist.entity.manytomanythirdtable;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class CourseRating {
    @EmbeddedId
    private CourseRatingKey id;
    @ManyToOne
    @MapsId("studentId")
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne
    @MapsId("courseId")
    @JoinColumn(name = "course_id")
    private Course course;
    private int rating;
    private LocalDate localDate = LocalDate.now();
    private boolean is_active;

}
