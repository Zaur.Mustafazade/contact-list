package az.atlacademy.contactlist.entity.manytomanythirdtable.dto;

import lombok.Data;

@Data
public class StudentDto {
    private Long id;
    private String name;
    private String surname;
}
