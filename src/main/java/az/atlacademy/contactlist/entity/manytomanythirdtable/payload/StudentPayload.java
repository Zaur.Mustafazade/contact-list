package az.atlacademy.contactlist.entity.manytomanythirdtable.payload;

import lombok.Data;

@Data
public class StudentPayload {
    private Long id;
    private String name;
    private String surname;
    private String patronymic;
}
