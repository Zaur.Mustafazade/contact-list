package az.atlacademy.contactlist.mapper;

import az.atlacademy.contactlist.entity.manytomanythirdtable.Student;
import az.atlacademy.contactlist.entity.manytomanythirdtable.dto.StudentDto;
import az.atlacademy.contactlist.entity.manytomanythirdtable.payload.StudentPayload;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper
public abstract class StudentMapper {
    public static final StudentMapper INSTANCE = Mappers.getMapper(StudentMapper.class);

    @Mapping(target = "id",source = "id")
    @Mapping(target = "name",source = "name")
    @Mapping(target = "surname",source = "surname")
    @Mapping(target = "patronymic",source = "patronymic")
    @Mapping(target = "fullName",source = "studentPayload" ,qualifiedByName = "getFullName")
    public abstract Student payloadToEntity(@Context StudentPayload studentPayload);

    @Mapping(target = "id",source = "id")
    @Mapping(target = "name",source = "name")
    @Mapping(target = "surname",source = "surname")
    public abstract StudentDto entityToDto(Student student);

    @Named("getFullName")
    private String getFullName(@Context StudentPayload studentPayload){
        String format="%s %s %s";
        return String.format(format,studentPayload.getName(),
                studentPayload.getSurname(),studentPayload.getPatronymic());
    }

}


//  concrete class      -    abstract class      -   interface
//  +concrete method     -     +abstract method        +public abstract void test();
                           //  +concrete meth0d

//int sum(int a, int  b){
//    return  a+b;
//}    -   concrete  method

//  abstract int sum(int a, int b);

