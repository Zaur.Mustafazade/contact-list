package az.atlacademy.contactlist.service;

import az.atlacademy.contactlist.entity.manytomanythirdtable.Student;
import az.atlacademy.contactlist.entity.manytomanythirdtable.dto.StudentDto;
import az.atlacademy.contactlist.entity.manytomanythirdtable.payload.StudentPayload;
import az.atlacademy.contactlist.mapper.StudentMapper;
import az.atlacademy.contactlist.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public List<StudentDto> findAll() {

       return studentRepository.findAll()
                .stream()
                .filter(s->!s.getIsDeleted())
                .map(s-> StudentMapper.INSTANCE.entityToDto(s))
                .collect(Collectors.toList());

    }

    public StudentDto create(StudentPayload studentPayload) {
        Student student = StudentMapper.INSTANCE.payloadToEntity(studentPayload);
        student.setIsDeleted(false);
        return StudentMapper.INSTANCE.entityToDto(studentRepository.save(student));
    }

    @Transactional
    public void delete(StudentPayload studentPayload) {
        Student student=studentRepository.getById(studentPayload.getId());
        student.setIsDeleted(true);   //soft delte e
        studentRepository.save(student);
       // studentRepository.delete(student); hard delete
    }
}
